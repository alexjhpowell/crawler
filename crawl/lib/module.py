from abc import ABCMeta
from requests import Response
from urlparse import urlparse
from urlparse import ParseResult


class Module(object):
    __metaclass__ = ABCMeta
    base_url = str
    base_url_components = ParseResult

    def __init__(self, base_url=str):
        self.base_url = base_url
        self.base_url_components = urlparse(base_url)

    # @abstractmethod
    def process(self, response=Response):
        return

    def can_process(self, response=Response):
        return True
