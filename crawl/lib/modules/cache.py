from requests import Response
from lib.module import Module


class Cache(Module):
    def process(self, response=Response):
        cache_headers = ['etag', 'cache-control', 'last-modified']
        result = {}
        for h in cache_headers:
            if h in response.headers:
                result[h.replace('-', '_')] = response.headers[h].strip('"')


        return result
