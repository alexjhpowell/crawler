from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class HtmlHeaders(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')

        headers = dom.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])
        result = []
        if headers is None:
            return result

        for h in headers:
            result.append({
                'tag_name': h.name,
                'text': h.get_text().strip() if h.get_text() is not None else '',
                'id': h['id'] if 'id' in h else ''
            })

        return {'headers': result}

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
