from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class AllText(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')

        body = dom.find('body')
        result = ''
        if body is None:
            return result

        result = body.get_text(separator="\n",strip=True)

        return {'text': result}

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
