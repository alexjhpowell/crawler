from lib.module import Module
from requests import Response
from bs4 import BeautifulSoup
import re


class InternalLinks(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')
        pattern = '^(\/|http(s)?://{}/)'.format(self.base_url_components.netloc)
        a = dom.find_all('a', href=re.compile(pattern, re.I))

        result = []
        if a is None:
            return result

        for l in a:
            link_href = l['href']
            link_text = l.get_text().strip() if l.get_text() is not None else ''
            link_title = l['title'].strip() if l['title'] is not None else ''
            link_nofollow = 'rel' in l and l['rel'].lower() == 'nofollow'

            if {'href': link_href, 'text': link_text, 'title': link_title, 'nofollow': link_nofollow} in result:
                continue

            result.append({
                'href': link_href,
                'text': link_text,
                'title': link_title,
                'nofollow': link_nofollow
            })

        return {'internal_links': result}

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
