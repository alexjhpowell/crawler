from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class Meta(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')
        pattern = re.compile('^(description|keywords)$', re.I)
        e = dom.find_all('meta', attrs={'name': pattern})

        result = {}
        for m in e:
            result.update({
                'meta_' + m['name'].lower(): m['content'],
                'meta_' + m['name'].lower() + '_length': len(m['content']),
            })

        return result

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
