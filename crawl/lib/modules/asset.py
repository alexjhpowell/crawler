from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class Asset(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')
        js_assets = dom.find_all('script', attrs={'src': re.compile('.*')})
        css_assets = dom.find_all('link', attrs={'href': re.compile('.*'), 'rel': 'stylesheet'})
        img_assets = dom.find_all('img')
        result = {
            'js': [],
            'css': [],
            'img': []
        }

        if js_assets is not None:
            for j in js_assets:
                result['js'].append(j['src'])

        if css_assets is not None:
            for c in css_assets:
                result['css'].append(c['href'])

        if img_assets is not None:
            for i in img_assets:
                result['img'].append(i['src'])

        return result

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
