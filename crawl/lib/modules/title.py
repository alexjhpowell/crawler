from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class Title(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')

        if dom.title is None:
            return {
                'title': '',
                'title_length': 0
            }

        return {
            'title': dom.title.string.strip(),
            'title_length': len(dom.title.string.strip())
        }

    def can_process(self, response=Response):
        return response.status_code == 200 and re.match('^text/html', response.headers['content-type'])
