from requests import Response
from lib.module import Module


class Status(Module):
    def process(self, response=Response):
        return {
            'status_code': response.status_code,
            'content_type': response.headers['content-type']
        }
