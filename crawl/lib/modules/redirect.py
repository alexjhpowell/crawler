from requests import Response
from lib.module import Module


class Redirect(Module):
    def process(self, response=Response):
        headers = response.headers
        if 'Location' in headers:
            return {
                'redirect': headers['Location']
            }

        return {}
