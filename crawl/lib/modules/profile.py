from requests import Response
from lib.module import Module


class Profile(Module):
    def process(self, response=Response):
        return {
            'ttfb': round(response.elapsed.total_seconds(), 3),
            'content_length': response.headers['content-length'] if 'content-length' in response.headers else len(response.content)
        }
