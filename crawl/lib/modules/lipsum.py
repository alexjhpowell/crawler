from requests import Response
from lib.module import Module
from bs4 import BeautifulSoup
import re


class Lipsum(Module):
    def process(self, response=Response):
        dom = BeautifulSoup(response.content, 'html.parser')
        elements = dom.find_all(None, text=re.compile('^Lorem ipsum dolor sit amet\s?.*'))

        result = []
        for e in elements:
            cap = e.text.rfind(' ', 100)
            result.append({
                'text': e.text[:cap] + '...' if len(e.text) > 100 else e.text,
                'tag': e.name
            })

        return result

    def can_process(self, response=Response):
        return re.match('^text/html.*', response.headers['content-type']) and len(response.content) > 0
