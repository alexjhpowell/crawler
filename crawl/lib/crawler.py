import importlib
import requests
import sys
from collections import deque
from bs4 import BeautifulSoup
import re
from urlparse import urlparse
from urlparse import ParseResult
from datetime import datetime
from module import Module
import threading
from . import __version__


class Crawler:
    base_url = str
    base_url_components = ParseResult
    modules = list  # type: list[str]
    traverse = bool
    output = file
    results = {}
    queued = deque()
    verbose = False

    def __init__(self, base_url=str, modules=list, traverse=bool, verbose=False, debug=False, filter=None):
        """
        Class constructor

        :param base_url: The url to begin with - typically this is the top level path on a domain
        :param modules: The modules to enable for this crawl
        :param traverse: Denote if the crawl should follow links to crawl a whole site
        :param verbose: Extra informational output
        :param debug: Debug output for development
        """
        self.set_base_url(base_url)
        self.modules = modules
        self.traverse = traverse
        self.verbose = verbose
        self.debug = debug
        self.queued.append(self.base_url)
        self.lock = threading.Lock()

    def crawl(self):
        """
        Execute a request for the next url in the queue.
        :rtype None
        :return:
        """
        url = self.queued.popleft()
        self.debug_output('do_request {url}'.format(url=url))
        response = False
        try:
            response = requests.get(url, allow_redirects=False, headers={'User-Agent': 'Crawler/%s' % __version__ })
            self.results[url] = {}
            for m in self.modules:
                try:

                    module = self.get_module(m)
                    if module.can_process(response):
                        self.results[url].update(module.process(response))
                except Exception as ex:
                    sys.stderr.write('Error processing response "{}" with "{}"\n'.format(url, m))
                    if self.verbose:
                        sys.stderr.write(ex.message + '\n')
        except requests.exceptions.TooManyRedirects as err:
            sys.stderr.write('Too many redirects trying to access URL: "{}"\n'.format(url))
        except requests.exceptions.ConnectionError as err:
            sys.stderr.write(err.message)
            self.results[url] = {
                'error': err.message
            }

        if self.traverse:
            if response and response.ok and urlparse(url).netloc == urlparse(self.base_url).netloc:
                # Don't follow URLs which are not on the same domain
                self.queue_links(url, response)

    def queue_links(self, url=str, response=requests.Response):
        """
        Queue up all the links which can be found on the page.
        If the response is a redirect we will also queue that up.

        :param url: The URL we're currently on.
        :param response: The Response object for the current request.
        :return:
        """
        self.debug_output('queue_links {url}'.format(url=url))
        headers = response.headers
        if re.match('^text/html.*', headers['content-type'], re.IGNORECASE) is not None and len(response.content) > 0:
            dom = BeautifulSoup(response.content, 'html.parser')
            path_match = re.compile('^(/|http(s)?://{}/|(?!(http(s)?|mailto:|tel:|#|javascript:)))'.format(self.base_url_components.netloc), re.IGNORECASE)
            elements = dom.find_all(['a', 'link'], attrs={'href': path_match})
            elements.extend(dom.find_all(['img', 'script'], attrs={'src': path_match}))

            for e in elements:
                attr = 'href' if e.has_attr('href') else 'src'
                self.debug_output('found_link {url}'.format(url=e[attr]))
                if e[attr].startswith('http'):
                    follow_url = e[attr]
                else:
                    follow_url = '{protocol}://{domain}/{href}'.format(protocol=self.base_url_components.scheme, domain=self.base_url_components.netloc, href=e[attr].lstrip('/'))

                if follow_url not in self.results and follow_url not in self.queued:
                    self.debug_output('queue_url {url}'.format(url=follow_url))
                    self.queued.append(follow_url)

        if response.is_redirect:
            redirect_url = headers['location']
            multiple_redirects = re.match('^(.*),\s*(http(s)?|/)', redirect_url, re.IGNORECASE)
            if multiple_redirects is not None:
                redirect_url = multiple_redirects.group(1)

            if redirect_url.startswith('/'):
                redirect_url = '{protocol}://{domain}/{href}'.format(protocol=self.base_url_components.scheme, domain=self.base_url_components.netloc, href=redirect_url.lstrip('/'))

            if redirect_url not in self.results and redirect_url not in self.queued:

                self.queued.append(redirect_url)

            if url == self.base_url:
                # Correct our base_url if the first thing we receive is a redirect.
                self.set_base_url(redirect_url)

    def has_queued(self):
        """
        Determine if the queue has any links left to crawl
        :rtype bool:
        :return:
        """
        return len(self.queued) > 0

    def get_module(self, name=str):
        """
        Resolve & return a class for the named module
        :rtype: Module
        :param name: Module name to retrieve the class for.
        :return: The module which has been requested.
        """
        class_name = name[0].upper() + name[1:].lower()
        class_name = re.sub('_([a-z]{1})', lambda pat: pat.group(1).upper(), class_name)
        module = importlib.import_module('lib.modules.' + name)
        return getattr(module, class_name)(self.base_url)

    def debug_output(self, log=str):
        """
        Print out debug information only when debug is enabled.

        :rtype None
        :param log: The message to print when debugging
        """
        if not self.debug:
            return
        with self.lock:
            print('DEBUG {time}: {message}'.format(message=log, time=datetime.now()))

    def set_base_url(self, url=str):
        """
        Set the base_url which will be used as reference for all future requests.

        :param url: The URL which we are going to set as our starting point.
        :return:
        """
        self.base_url = url
        self.base_url_components = urlparse(url)
