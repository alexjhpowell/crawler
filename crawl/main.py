#! /usr/bin/env python

import sys
import argparse
from lib.crawler import Crawler
import pkgutil
import lib.modules
import json
import progressbar
from time import sleep
import threading

__author__ = 'Alex'
usage = 'bin/crawl [URL]'
description = 'A command line utility to scrape information from web pages.'


def crawl_threaded():
    threads = []
    max_threads = args.threads if len(crawler.queued) > args.threads else len(crawler.queued)
    for i in range(max_threads):
        if args.debug:
            sys.stdout.write('Creating thread %s\n' % (i + 1))
        t = threading.Thread(target=crawler.crawl)
        t.setDaemon(True)
        threads.append(t)
        t.start()
        t.join(2)


try:
    mods = []
    for importer, modname, ispkg in pkgutil.iter_modules(lib.modules.__path__):
        mods.append(modname)

    parser = argparse.ArgumentParser(usage=usage, description=description)
    parser.add_argument('url', metavar='URL', type=str, help='The root URL to scrape.')
    parser.add_argument('-o', '--output', metavar='file', default=sys.stdout, type=str, help='Optional output file')
    parser.add_argument('-m', '--modules', nargs='+', choices=mods + ['ALL'], default=['status', 'redirect'],
                        help='Modules to enable when crawling')
    parser.add_argument('--no-traverse', default=True, action='store_false',
                        help='Optionally disable traversing pages. With this flag only the specified page is crawled.')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('--debug', default=False, action='store_true')
    parser.add_argument('-t', '--threads', default=5, type=int, help='The number of threads to use when crawling.')
    parser.add_argument('-n', '--throttle', default=0.5, type=float, help='Delay between batches of requests in seconds. Increase if you are seeing excessive load on the target server.')

    args = parser.parse_args()
    if 'ALL' in args.modules:
        args.modules = mods

    crawler = Crawler(base_url=args.url, modules=args.modules, traverse=args.no_traverse, verbose=args.verbose, debug=args.debug)
    bar = progressbar.ProgressBar(min_value=0, max_value=len(crawler.queued), poll_interval=1)

    while crawler.has_queued():
        if args.threads > 1:
            sleep(args.throttle)  # Make sure we don't inadvertently flood the target
            try:
                crawl_threaded()
            except Exception as ex:
                sys.stderr.write('There was an error processing a response: "%s"' % [ex.message])
        else:
            crawler.crawl()
        bar.max_value = len(crawler.queued) + len(crawler.results)
        bar.update(len(crawler.results))

    result = crawler.results
    output = args.output
    if type(output) is str:
        output = open(output, 'w')
    output = output
    json.dump(result, output)
except KeyboardInterrupt:
    print('User aborted.')
