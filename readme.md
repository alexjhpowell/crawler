# Website Crawler/Scraper

## What is This?

This is a tool used to gather useful metrics from a web page or whole site that would otherwise be quite laborious to obtain.

## This is Not

- A performance benchmarking/stress testing tool ([siege](http://linux.die.net/man/1/siege))
- A page speed checking tool ([Google Page Speed Insights](https://developers.google.com/speed/pagespeed/insights/), [YSlow](http://yslow.org/))
- Going to use selenium or any other headless browser (without a damned good reason).

## Requirements

- Python 2.7
- Pip Packages (installed by setup)
    - beautifulsoup4

## Installation

Download the files anywhere on your local system and run ./bin/setup from the top level directory of the project.

## Usage

```
./bin/crawl [url] -m [modules you want to report with] [--debug] [--verbose] [-o|output file]
```

The only current output format is JSON. If you are running this without an output file you can pipe the result back to python to pretty print the result like so:
```
./bin/crawl [url] -m [modules you want to report with] | python -m json.tool
```

## To Do

### Output Formats

I want to offer more than just JSON, potentially will be investigating XML as an option.

**CSV will NEVER be a supported option.** The web is not flat - spreadsheets should not be used to manage it and should die in a fire.

### Moar Reports

Any report which could provide useful metrics without massively altering the tool will be considered.